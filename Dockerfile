# Start with the alpine linux tomcat image
FROM tomcat:8.5-jdk8-openjdk

# Change to the CATALINA_HOME directory
WORKDIR $CATALINA_HOME

# Create a reference to some file on the physical machine (optional)
ARG WAR_FILE=./target/*.war

# Copy the file from the physical machine to the webapps dir in the PWD
COPY $WAR_FILE ./webapps/ROOT.war

# Expose port 8080
EXPOSE 8080

# Execute the command that starts tomcat
CMD ["catalina.sh", "run"]